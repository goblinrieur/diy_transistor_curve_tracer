# Curve tracer

this device draws transistor curves on XY mode oscilloscope.

[schema](https://gitlab.com/goblinrieur/diy_transistor_curve_tracer/-/blob/master/FULL.png)

[UPside](https://gitlab.com/goblinrieur/diy_transistor_curve_tracer/-/blob/master/FACE_UP.png)

[Downside](https://gitlab.com/goblinrieur/diy_transistor_curve_tracer/-/blob/master/FACE_DOWN.png)

[files](https://gitlab.com/goblinrieur/diy_transistor_curve_tracer/-/tree/master/)

[prototype test video](https://www.youtube.com/watch?v=AT7KfzAuBrY)
                                                                                                                                                                                           
## sur la chaine youtube je montre certains de projets                                                                                                                                       
                                                                                                                                                                                             
[youtube](https://www.youtube.com/channel/UCnsW_UH9vXX-nBe3X-enXYA)                                                                                                                          

on peut aussi me contacter sur : [goblinrieur@gmail.com](mailto://goblinrieur@gmail.com)

ou on peut me joindre sur discord : [discord](https://discord.gg/9szvduB)

